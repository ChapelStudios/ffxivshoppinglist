; (function () {
    var vm = null;
    const ViewModal1 = function () {
        let self = this;
        
        self.property = 1;
        self.tell = function() {
            return self.property;
        }
    };
    
    Window.VM1ExposedObj = {
        init: function() {
            vm = new ViewModal1();
        },
        tell: function() {
            return vm.tell();
        }
    };
})()