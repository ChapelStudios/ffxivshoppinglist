; (function () {
    var vm = null;
    const ViewModal2 = function () {
        let self = this;
        
        self.property = 2;
        self.tell = function() {
            return self.property;
        }
    };
    
    Window.VM2ExposedObj = {
        init: function() {
            vm = new ViewModal2();
        },
        tell: function() {
            return vm.tell();
        }
    };
})()