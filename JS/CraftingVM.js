; (function () {
    // Constants and Private Vars
    function noop() { };
    const DEFAULT_CONFIGURATION = {
        Tabs: DEFAULT_TABS,
        DefaultTab: "Master",
        OpenAddItemModal: noop,
        ReadCookie: noop,
        WriteToCookie: noop,
    };
    const MakeTabs = function(tabs){
        let r = [];
        _.each(tabs, function(tab){
            r.push(new TabDataSet(tab));
        });
        return r;
    };

    let vm = null;
    let isInit = false;

    // VM
    const CraftingVM = function (config) {
        let self = this;

        
        // Config
        let cookieData = config.ReadCookie();
        self.Tabs = ko.observableArray(cookieData || MakeTabs(config.Tabs));
        const openAddItemModal = config.OpenAddItemModal;
        self.Save = function (){
            config.WriteToCookie(self.Tabs());
        };

        //Data Binds
        self.ChooseLogHeaderTemplate = function(tabData){
            let tName = tabData.Name;
            if (GATHERING_TABS.includes(tName)){
                return "GatheringLog_Header_Template";
            }
            else {
                return "Empty_Template"
            }
        };
        self.ChooseLogItemTemplate = function(logItem){
            let tName = logItem.CreatedBy;
            if (GATHERING_TABS.includes(tName)){
                return "GatheringLog_Item_Template";
            }
            else {
                return "Empty_Template"
            }
        };

        // Public Functions
        self.ChangeTab = function(tabData){
            self.Tabs().forEach(t => {
                t.IsActive(t.Name === tabData.Name);
            });
        };
        self.OpenAddItemModal = function(tabData) {
            openAddItemModal(tabData, self.Tabs());
        };
        self.AddItem = function(item) {
            let t = _.find(self.Tabs(), function(tab){
                return tab.Name === item.CreatedBy;
            });
            
            t.Items.push(item);
        };
    };

    // Global Access
    Window.CraftingVM = {
        Init: function(config) {
            if (!isInit){
                let cfg = _.extend({}, DEFAULT_CONFIGURATION, config);
                vm = new CraftingVM(cfg);
                ko.applyBindings(vm, document.getElementById(cfg.BindingHandler));
                vm.ChangeTab({Name: cfg.DefaultTab});

                isInit = true;
            }
        },
        Save: function(item) {
            if (item.Name !== "")
            {
                vm.AddItem(item);
            }
            vm.Save();
        }
    };
})();