// Page Constants
const DEFAULT_TABS = [
    "Master", "Gathering", "BTN", "MIN", "FSH", "Shops", "Drops",
    "CAR", "BSM", "ARM", "GSM", "LTW", "WVR", "ALC", "CUL",
];
const GATHERING_TABS = [
    "BTN", "MIN", "FSH"
];
const CRAFTING_TABS = [
    "CAR", "BSM", "ARM", "GSM", "LTW", "WVR", "ALC", "CUL"
];
const DEFAULT_ADDMODES = [
    "Gathering",
    "Crafting",
    "Master",
];

// Models
const TabDataSet = function(name, isActive, items){
    let self = this;

    self.IsActive = ko.observable(isActive || false);
    self.Name = name;
    self.Items = ko.observableArray(items || []);
};

const DEFAULT_LOGITEM_CONFIG = {
    Name: "",
    ID: "",
    RawHQ: 0,
    RawLQ: 0,
    CompHQ: 0,
    CompLQ: 0,
    HaveHQ: 0,
    HaveLQ: 0,
    Location: "",
    Level: "",
    Notes: "",
    CreatedBy: "",
};
const LogItemVM = function(cfg){
    let self = this;
    self = _.extend(self, DEFAULT_LOGITEM_CONFIG, cfg);
};