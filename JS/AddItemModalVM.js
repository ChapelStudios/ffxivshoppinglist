; (function () {
    // Constants and Private Vars
    function noop() { };
    const DEFAULT_ADDMODAL_CONFIG = {
        Mode: DEFAULT_ADDMODES[0],
        BindingHandler: "AddLogItem_Modal",
        SaveCallback: noop,
    };

    let vm = null;
    let isInit = false;

    const AddItemModalVM = function(config){
        self = this;

        // Config
        self.Mode = ko.observable(config.Mode);
        self.IsVisible = ko.observable(false);
        self.ActiveTabType = ko.observable("");
        let saveCallback = config.SaveCallback;

        //Data Binds
        self.AddTypeText = function(){
            if (self.ActiveTabType() === "Master") {
                return "Add an item to your Master List!"
            }
            else {
                return "Add a " + self.Mode() + " item from " + self.ActiveTabType() + " to your log!"
            }
        };
        self.ItemName = ko.observable("");
        self.ItemLocation = ko.observable("");
        self.ItemLevel = ko.observable("");
        self.ItemNotes = ko.observable("");

        self.SaveItem = function() {
            let item = {
                Name: self.ItemName(),
                Location: self.ItemLocation(),
                Level: self.ItemLevel(),
                Notes: self.ItemNotes(),
                CreatedBy: self.ActiveTabType(),
            }
            let log = new LogItemVM(item);
            saveCallback(log);
            self.Hide();
        };

        // Modal Functionality
        window.onclick = function(event){
            if (event.target === document.getElementById(config.BindingHandler)) {
                self.Hide();
            }
        };
        self.Hide = function(){
            self.IsVisible(false);
        };
    };

    // Global Access
    Window.AddItemModalVM = {
        Init: function(config) {
            if (!isInit){
                let cfg = _.extend({}, DEFAULT_ADDMODAL_CONFIG, config);
                vm = new AddItemModalVM(cfg);
                ko.applyBindings(vm, document.getElementById(cfg.BindingHandler));

                isInit = true;
            }
        },
        Show: function(tabName, itemData){
            vm.ActiveTabType(tabName);

            if (GATHERING_TABS.includes(tabName)){
                vm.Mode("Gathering")
            }
            else if (CRAFTING_TABS.includes(tabName)){
                vm.Mode("Crafting")
            }
            else if (tabName === "Master"){
                vm.Mode("Master")
            }
            else {
                //Not Implemented!
            }
            
            if (itemData){
                vm.ItemName(itemData);
                vm.ItemLocation(itemData);
                vm.ItemLevel(itemData);
                vm.ItemNotes(itemData);
            }

            vm.IsVisible(true);
        },
        Hide: function(){
            vm.Hide();
        },
    };
})();