; (function () {
    // Constants and Private Vars
    const COOKIENAME = "XIVCraft";
    // Cookie Functionality
    let saveCookie = function(tabs){
        let cookie = [
            COOKIENAME,
            '=',
            JSON.stringify(ko.toJS(tabs)),
            //'; domain=FFXIV.craft',
            '; path=/;'
        ].join('');
        document.cookie = cookie;
    };

    let readCookie = function() {
        let result = document.cookie.match(new RegExp(COOKIENAME + '=([^;]+)'));
        result && (result = JSON.parse(result[1]));
        return result;
    };

    let wrapItems = function(items) {
        return items;
    };

    let wrapCookie = function (cookieData){
        let wrappedTabs = [];
        cookieData.forEach(cookieTab => {
            let items = wrapItems(cookieTab.Items)
            let tabTemp = new TabDataSet(cookieTab.Name, cookieTab.IsActive, items);
            wrappedTabs.push(tabTemp);
        });
        return wrappedTabs;
    };

    Window.Bakery = {
        Bake: function(tabs) {
            saveCookie(tabs);
        },
        Consume: function(){
            let c = readCookie();
            if (c) {
                return wrapCookie(c);
            }
            else { 
                return;
            }
        },
    };

})();